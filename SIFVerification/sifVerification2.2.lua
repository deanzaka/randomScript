DATA_DIR = "data/"
DATA_DIR = "VERIFICATION_SAMPLE/"
SPEC_DIR = "spec/"

T_FILENAME = {
"DL_TARGET_CONTRACT_AXA_DMTM_Delta_PartyContractComponent_04082015.sif";
"DL_TARGET_CONTRACT_AXA_DMTM_Delta_PartyContractPartyRole_04082015.sif";
"DL_TARGET_CONTRACT_AXA_DMTM_Delta_PartyContract_04082015.sif";
"DL_TARGET_CONTRACT_AXA_RLS_Delta_PartyContractComponent_04082015.sif";
"DL_TARGET_CONTRACT_AXA_RLS_Delta_PartyContractPartyRole_04082015.sif";
"DL_TARGET_CONTRACT_AXA_RLS_Delta_PartyContract_04082015.sif";
"DL_TARGET_CONTRACT_BSM_Delta_PartyContractComponent_04082015.sif";
"DL_TARGET_CONTRACT_BSM_Delta_PartyContractMiscValue_04082015.sif";
"DL_TARGET_CONTRACT_BSM_Delta_PartyContractPartyRole_04082015.sif";
"DL_TARGET_CONTRACT_BSM_Delta_PartyContract_04082015.sif";
"DL_TARGET_CONTRACT_EMAS_Delta_PartyContractComponent_04082015.sif";
"DL_TARGET_CONTRACT_EMAS_Delta_PartyContractPartyRole_04082015.sif";
"DL_TARGET_CONTRACT_EMAS_Delta_PartyContract_04082015.sif";
"DL_TARGET_CONTRACT_MTF_Delta_PartyContractComponent_04082015.sif";
"DL_TARGET_CONTRACT_MTF_Delta_PartyContractPartyRole_04082015.sif";
"DL_TARGET_CONTRACT_MTF_Delta_PartyContract_04082015.sif";
"DL_TARGET_PARTY_AXA_DMTM_Combined_Contact_Org.sif";
"DL_TARGET_PARTY_AXA_DMTM_Combined_Contact_Person.sif";
"DL_TARGET_PARTY_AXA_DMTM_Delta_PartyAddress_04082015.sif";
"DL_TARGET_PARTY_AXA_DMTM_Reject_Contact.sif";
"DL_TARGET_PARTY_AXA_DMTM_Reject_OrgName.sif";
"DL_TARGET_PARTY_AXA_DMTM_Reject_PersonName.sif";
"DL_TARGET_PARTY_AXA_RLS_Combined_Contact_Org.sif";
"DL_TARGET_PARTY_AXA_RLS_Combined_Contact_Person.sif";
"DL_TARGET_PARTY_AXA_RLS_Delta_PartyAddress_04082015.sif";
"DL_TARGET_PARTY_AXA_RLS_Delta_PartyIdentification_04082015.sif";
"DL_TARGET_PARTY_AXA_RLS_Delta_PartyMiscValue_04082015.sif";
"DL_TARGET_PARTY_AXA_RLS_Reject_Contact.sif";
"DL_TARGET_PARTY_AXA_RLS_Reject_PersonName.sif";
"DL_TARGET_PARTY_BSM_Combined_Contact_Org.sif";
"DL_TARGET_PARTY_BSM_Combined_Contact_Person.sif";
"DL_TARGET_PARTY_BSM_Delta_PartyAddress_04082015.sif";
"DL_TARGET_PARTY_BSM_Delta_PartyIdentification_04082015.sif";
"DL_TARGET_PARTY_BSM_Delta_PartyMiscValue_04082015.sif";
"DL_TARGET_PARTY_BSM_Reject_Contact.sif";
"DL_TARGET_PARTY_BSM_Reject_OrgName.sif";
"DL_TARGET_PARTY_BSM_Reject_PersonName.sif";
"DL_TARGET_PARTY_EMAS_Combined_Contact_Org.sif";
"DL_TARGET_PARTY_EMAS_Combined_Contact_Person.sif";
"DL_TARGET_PARTY_EMAS_Delta_PartyAddress_04082015.sif";
"DL_TARGET_PARTY_EMAS_Delta_PartyIdentification_04082015.sif";
"DL_TARGET_PARTY_EMAS_Delta_PartyMiscValue_04082015.sif";
"DL_TARGET_PARTY_EMAS_Reject_Contact.sif";
"DL_TARGET_PARTY_EMAS_Reject_OrgName.sif";
"DL_TARGET_PARTY_EMAS_Reject_PersonName.sif";
"DL_TARGET_PARTY_MTF_Combined_Contact_Org.sif";
"DL_TARGET_PARTY_MTF_Combined_Contact_Person.sif";
"DL_TARGET_PARTY_MTF_Delta_PartyAddress_04082015.sif";
"DL_TARGET_PARTY_MTF_Delta_PartyIdentification_04082015.sif";
"DL_TARGET_PARTY_MTF_Delta_PartyMiscValue_04082015.sif";
"DL_TARGET_PARTY_MTF_Reject_Contact.sif";
"DL_TARGET_PARTY_MTF_Reject_OrgName.sif";
"DL_TARGET_PARTY_MTF_Reject_PersonName.sif";
--]=]
}

--libraries 1
function table.join(t1, t2)
  local t1Len = #t1
  for i = 1, #t2 do
    t1[t1Len+i] = t2[i]
  end
  return t1
end

--process specification file e.g. Contract.sif
function getSpecTable(filename)
  local t = {}
  local fSpec = io.open(filename, "r")
  if not fSpec then print(string.format("[WARNING] Specification file %s cannot be found.", filename)); return nil; end
  local colCount = 1
  for line in fSpec:lines() do
    if not (string.match(line, "^%s*$") or string.match(line, "^%s*#")) then
      local colName, isNullable, businessFieldName = string.match(line, "([^%s]+)%s+([^%s]*)%s*([^%s]*)")
      --print(colCount, colName, isNullable, businessFieldName)
      t[colCount] = {
        colName = colName;
        isNullable = isNullable;
        businessFieldName = businessFieldName;
      }
      colCount = colCount + 1
    end
  end
  t.origColCount = colCount - 1
  fSpec:close()
  return t
end

--process SIF.properties
local tSpec = {}
local fProperties = io.open(string.format("%s/%s", SPEC_DIR, "SIF.properties"), "r")
if not fProperties then print("[WARNING] Specification SIF.properties cannot be found.") end
  local specCount = 1
  for line in fProperties:lines() do
    --e.g. line = "metadata.P.P=PartyContact.sif"
    local key, specFilename = string.match(line, "^%s*metadata%.(.%..)%s*=%s*([%w%.]+)")
    if key then
      --print(key, specFilename)
      tSpec[key] = getSpecTable(string.format("%s/%s", SPEC_DIR, specFilename))
      
      --exception override for P.P, combine with P.H
      if (key == "P.P" or key == "P.H") and (tSpec["P.P"] and tSpec["P.H"]) then
        table.join(tSpec["P.P"], tSpec["P.H"])
      end
      if (key == "P.O" or key == "P.G") and (tSpec["P.O"] and tSpec["P.G"]) then
        table.join(tSpec["P.O"], tSpec["P.G"])
      end
      
      if tSpec[key] then specCount = specCount + 1 end
    end
  end
  print(string.format("[INFO] %d sif specification files loaded.", specCount - 1))
  print()
fProperties:close()

--check spec table
--[[
for k, t in pairs(tSpec) do
  for i, v in ipairs(t) do
    print(k, i, v.colName, v.isNullable, v.businessFieldName)
  end
end
--]]

--libraries 2
function split(s, r)
  local t = {}
  for v in string.gmatch(s, string.format("([^%s]*)%s", r, r)) do
    table.insert(t, v)
  end
  return t
end
function checkValueExists(v, tSpecKeyI)
  local isExists = true
  if tSpecKeyI.isNullable == "N" then
    if (not v) or (v == "") then
      isExists = false
    end
  end
  return isExists
end

--processing input sif files
for i, filename in ipairs(T_FILENAME) do
  print(string.format("[INFO] Processing file [%s].", filename))
  local isRejectFile = false
  if string.match(string.lower(filename), "reject") then isRejectFile = true end
  
  local fi = io.open(string.format("%s/%s", DATA_DIR, filename), "r")
  local lineCount = 1
  for line in fi:lines() do
  
  
    local tLine = split(line, "|")
    local key = string.format("%s.%s", tLine[1], tLine[2])
    local tSpecKey = tSpec[key]
    --print(line)
    --print(key, #tSpecKey, #t)
    local expectedSIFSize = isRejectFile and tSpecKey.origColCount or #tSpecKey
    local lineSize = #tLine
    if expectedSIFSize == lineSize then
      
      for i, v in ipairs(tLine) do
        local tSpecKeyI = tSpecKey[i]
        
        --CHECKING STARTS HERE
        local isExists = checkValueExists(v, tSpecKeyI)
        if not isExists then
          print(string.format("[ERR_2_SPEC] File [%s] at line [%d] has no value for %s (col [%d]).", filename, lineCount, tSpecKeyI.colName, i))
        end
      end
      
    else
      print(string.format("[ERR_1_SIZE] SIF size of [%s] at line [%d] is incorrect (%d instead of %d).", filename, lineCount, lineSize, expectedSIFSize))
    end
    lineCount = lineCount + 1
    --if lineCount > 1 then break end--STOP
  
  
  end
  print()
  fi:close()
  --break--STOP
end
  