DATA_DIR = "VERIFICATION_SAMPLE/"
SPEC_DIR = "spec/"

--reference code table
require("refTable_R1V19_cleanedup_colNameAsKey")
require("refTable_R1V19_cleanedup_colNameAsKey_ootb")

--list of input filename
T_FILENAME = {
--[["head5_AXA_DMTM_Combined_Contact_Person.sif";
"head5_AXA_DMTM_PartyAddress_04082015.sif";
"head5_BSM_OrganizationName_04082015.sif";
"head5_AXA_DMTM_PartyContactMethod_04082015.sif";
"head5_AXA_DMTM_PersonName_04082015.sif";
--]]
"DL_TARGET_CONTRACT_AXA_DMTM_Delta_PartyContractComponent_04082015.sif";
"DL_TARGET_CONTRACT_AXA_DMTM_Delta_PartyContractPartyRole_04082015.sif";
"DL_TARGET_CONTRACT_AXA_DMTM_Delta_PartyContract_04082015.sif";
"DL_TARGET_CONTRACT_AXA_RLS_Delta_PartyContractComponent_04082015.sif";
"DL_TARGET_CONTRACT_AXA_RLS_Delta_PartyContractPartyRole_04082015.sif";
"DL_TARGET_CONTRACT_AXA_RLS_Delta_PartyContract_04082015.sif";
"DL_TARGET_CONTRACT_BSM_Delta_PartyContractComponent_04082015.sif";
"DL_TARGET_CONTRACT_BSM_Delta_PartyContractMiscValue_04082015.sif";
"DL_TARGET_CONTRACT_BSM_Delta_PartyContractPartyRole_04082015.sif";
"DL_TARGET_CONTRACT_BSM_Delta_PartyContract_04082015.sif";
"DL_TARGET_CONTRACT_EMAS_Delta_PartyContractComponent_04082015.sif";
"DL_TARGET_CONTRACT_EMAS_Delta_PartyContractPartyRole_04082015.sif";
"DL_TARGET_CONTRACT_EMAS_Delta_PartyContract_04082015.sif";
"DL_TARGET_CONTRACT_MTF_Delta_PartyContractComponent_04082015.sif";
"DL_TARGET_CONTRACT_MTF_Delta_PartyContractPartyRole_04082015.sif";
"DL_TARGET_CONTRACT_MTF_Delta_PartyContract_04082015.sif";
"DL_TARGET_PARTY_AXA_DMTM_Combined_Contact_Org.sif";
"DL_TARGET_PARTY_AXA_DMTM_Combined_Contact_Person.sif";
"DL_TARGET_PARTY_AXA_DMTM_Delta_PartyAddress_04082015.sif";
"DL_TARGET_PARTY_AXA_DMTM_Reject_Contact.sif";
"DL_TARGET_PARTY_AXA_DMTM_Reject_OrgName.sif";
"DL_TARGET_PARTY_AXA_DMTM_Reject_PersonName.sif";
"DL_TARGET_PARTY_AXA_RLS_Combined_Contact_Org.sif";
"DL_TARGET_PARTY_AXA_RLS_Combined_Contact_Person.sif";
"DL_TARGET_PARTY_AXA_RLS_Delta_PartyAddress_04082015.sif";
"DL_TARGET_PARTY_AXA_RLS_Delta_PartyIdentification_04082015.sif";
"DL_TARGET_PARTY_AXA_RLS_Delta_PartyMiscValue_04082015.sif";
"DL_TARGET_PARTY_AXA_RLS_Reject_Contact.sif";
"DL_TARGET_PARTY_AXA_RLS_Reject_PersonName.sif";
"DL_TARGET_PARTY_BSM_Combined_Contact_Org.sif";
"DL_TARGET_PARTY_BSM_Combined_Contact_Person.sif";
"DL_TARGET_PARTY_BSM_Delta_PartyAddress_04082015.sif";
"DL_TARGET_PARTY_BSM_Delta_PartyIdentification_04082015.sif";
"DL_TARGET_PARTY_BSM_Delta_PartyMiscValue_04082015.sif";
"DL_TARGET_PARTY_BSM_Reject_Contact.sif";
"DL_TARGET_PARTY_BSM_Reject_OrgName.sif";
"DL_TARGET_PARTY_BSM_Reject_PersonName.sif";
"DL_TARGET_PARTY_EMAS_Combined_Contact_Org.sif";
"DL_TARGET_PARTY_EMAS_Combined_Contact_Person.sif";
"DL_TARGET_PARTY_EMAS_Delta_PartyAddress_04082015.sif";
"DL_TARGET_PARTY_EMAS_Delta_PartyIdentification_04082015.sif";
"DL_TARGET_PARTY_EMAS_Delta_PartyMiscValue_04082015.sif";
"DL_TARGET_PARTY_EMAS_Reject_Contact.sif";
"DL_TARGET_PARTY_EMAS_Reject_OrgName.sif";
"DL_TARGET_PARTY_EMAS_Reject_PersonName.sif";
"DL_TARGET_PARTY_MTF_Combined_Contact_Org.sif";
"DL_TARGET_PARTY_MTF_Combined_Contact_Person.sif";
"DL_TARGET_PARTY_MTF_Delta_PartyAddress_04082015.sif";
"DL_TARGET_PARTY_MTF_Delta_PartyIdentification_04082015.sif";
"DL_TARGET_PARTY_MTF_Delta_PartyMiscValue_04082015.sif";
"DL_TARGET_PARTY_MTF_Reject_Contact.sif";
"DL_TARGET_PARTY_MTF_Reject_OrgName.sif";
"DL_TARGET_PARTY_MTF_Reject_PersonName.sif";
--]=]
}

--libraries 1
function table.join(t1, t2)
  local t1Len = #t1
  for i = 1, #t2 do
    t1[t1Len+i] = t2[i]
  end
  return t1
end

tColNameMapping = {
  ["C.C.ADMIN_SYS_TP_CD"] = "ADMIN_FLD_NM_TP_CD";
  ["C.L.ADMIN_SYS_TP_CD"] = "ADMIN_FLD_NM_TP_CD";
  ["C.R.ADMIN_SYS_TP_CD"] = "ADMIN_FLD_NM_TP_CD";
  ["C.L.ADMIN_CLIENT_SYS_TP_CD"] = "ADMIN_SYS_TP_CD";
  ["C.R.ADMIN_CLIENT_SYS_TP_CD"] = "ADMIN_SYS_TP_CD";
  ["P.I.ASSIGNEDBY_ADMIN_SYS_TP_CD"] = "ADMIN_SYS_TP_CD";
  ["P.O.BIRTHPLACE_TP_CD"] = "COUNTRY_TP_CD";
  ["P.P.BIRTHPLACE_TP_CD"] = "COUNTRY_TP_CD";
  ["P.O.CITIZENSHIP_TP_CD"] = "COUNTRY_TP_CD";
  ["P.P.CITIZENSHIP_TP_CD"] = "COUNTRY_TP_CD";
  ["C.H.CONTR_LANG_TP_CD"] = "LANG_TP_CD";
  ["P.O.EXT_EMP_INDUSTRY_TP_CD"] = "INDUSTRY_TP_CD";
  ["P.P.EXT_EMP_INDUSTRY_TP_CD"] = "INDUSTRY_TP_CD";
  ["P.O.EXT_OCCUPATION_TP_CD"] = "OCCUPATION_TP_CD";
  ["P.P.EXT_OCCUPATION_TP_CD"] = "OCCUPATION_TP_CD";
  ["C.H.EXT_REL_BANK_TP_CD"] = "REL_WITH_BANK_TP_CD";
  ["P.O.PREF_LANG_TP_CD"] = "LANG_TP_CD";
  ["P.P.PREF_LANG_TP_CD"] = "LANG_TP_CD";
  ["P.O.PRVBY_ADMIN_SYS_TP_CD"] = "ADMIN_SYS_TP_CD";
  ["P.P.PRVBY_ADMIN_SYS_TP_CD"] = "ADMIN_SYS_TP_CD";
  ["C.H.REPLBY_ADMIN_SYS_TP_CD"] = "ADMIN_SYS_TP_CD";
  ["C.C.SERV_ARRANGE_TP_CD"] = "ARRANGEMENT_TP_CD";
}
--process specification file e.g. Contract.sif
function getSpecTable(filename, key)
  local t = {}
  local fSpec = io.open(filename, "r")
  if not fSpec then print(string.format("[WARNING] Specification file %s cannot be found.", filename)); return nil; end
  local colCount = 1
  for line in fSpec:lines() do
    if not (string.match(line, "^%s*$") or string.match(line, "^%s*#")) then
      local colName, isNullable, businessFieldName = string.match(line, "([^%s]+)%s+([^%s]*)%s*([^%s]*)")
      
      --SIF colName Override
      colName = tColNameMapping[key .. "." .. colName] or colName
      
      --print(colCount, colName, isNullable, businessFieldName)
      t[colCount] = {
        colName = colName;
        isNullable = isNullable;
        businessFieldName = businessFieldName;
      }
      colCount = colCount + 1
    end
  end
  t.origColCount = colCount - 1
  fSpec:close()
  return t
end

--process SIF.properties
local tSpec = {}
local fProperties = io.open(string.format("%s/%s", SPEC_DIR, "SIF.properties"), "r")
if not fProperties then print("[WARNING] Specification SIF.properties cannot be found.") end
  local specCount = 1
  for line in fProperties:lines() do
    --e.g. line = "metadata.P.P=PartyContact.sif"
    local key, specFilename = string.match(line, "^%s*metadata%.(.%..)%s*=%s*([%w%.]+)")
    if key then
      --print(key, specFilename)
      tSpec[key] = getSpecTable(string.format("%s/%s", SPEC_DIR, specFilename), key)
      
      --exception override for P.P, combine with P.H
      if (key == "P.P" or key == "P.H") and (tSpec["P.P"] and tSpec["P.H"]) then
        table.join(tSpec["P.P"], tSpec["P.H"])
      end
      if (key == "P.O" or key == "P.G") and (tSpec["P.O"] and tSpec["P.G"]) then
        table.join(tSpec["P.O"], tSpec["P.G"])
      end
      
      if tSpec[key] then specCount = specCount + 1 end
    end
  end
  print(string.format("[INFO] %d sif specification files loaded.", specCount - 1))
  print()
fProperties:close()

--check spec table
--[[
for k, t in pairs(tSpec) do
  for i, v in ipairs(t) do
    if (string.find(v.colName, "_TP_CD$") or string.find(v.colName, "_TP_CODE$")) and not string.find(v.colName, "^NULL_") then
      print(tRef[v.colName] and "" or "X", v.colName, k, i, v.isNullable, v.businessFieldName)
      --print(k, i, v.colName, v.isNullable, v.businessFieldName)
    end
  end
  print()
end
--]]

--libraries 2
function split(s, r)
  local t = {}
  for v in string.gmatch(s, string.format("([^%s]*)%s", r, r)) do
    table.insert(t, v)
  end
  return t
end
--2 - NULLABLE
tMandatoryOveride = {
  ["P.S.SOURCE_IDENT_TP_CD"] = true;
  ["P.H.SOURCE_IDENT_TP_CD"] = true;
  ["P.M.SOURCE_IDENT_TP_CD"] = true;
  ["P.I.SOURCE_IDENT_TP_CD"] = true;
  ["P.O.SOURCE_IDENT_TP_CD"] = true;
  ["P.C.SOURCE_IDENT_TP_CD"] = true;
  ["P.P.SOURCE_IDENT_TP_CD"] = true;
  ["P.A.SOURCE_IDENT_TP_CD"] = true;
  ["P.G.SOURCE_IDENT_TP_CD"] = true;
  ["P.P.BIRTH_DT"] = true;
  ["P.O.ESTABLISHED_DT"] = true;
}
function checkValueExists(v, tSpecKeyI, key)
  local isExists = true
  if tSpecKeyI.isNullable == "N" or tMandatoryOveride[key .. "." .. tSpecKeyI.colName] then
    if (not v) or (v == "") then
      isExists = false
    end
  end
  return isExists
end
--3 - DATE FORMAT
tDDUL = {
  [1] = 31;
  [2] = 28;
  [3] = 31;
  [4] = 30;
  [5] = 31;
  [6] = 30;
  [7] = 31;
  [8] = 31;
  [9] = 30;
  [10] = 31;
  [11] = 30;
  [12] = 31;
  [13] = 29;--leap year
}
function checkDate(v, tSpecKeyI)
  local dateProblem = false
  local colName = tSpecKeyI.colName
  if (v) and (v ~= "") then
    if not(string.find(string.upper(colName), "^NULL_")) and (string.find(string.upper(colName), "DATE$") or string.find(string.upper(colName), "DT$")) then
      local yyyy, mm, dd = string.match(v, "(%d%d%d%d)%-(%d%d)%-(%d%d)")
      yyyy = tonumber(yyyy)
      mm = tonumber(mm)
      dd = tonumber(dd)
      if yyyy and mm and dd then
        if mm > 12 or mm < 1 then
          dateProblem = "wrong month"
        end
        --leap year handler
        if (yyyy%4 == 0) and mm == 2 then
          mm = 13
        end
        local ddUpperLimit = tDDUL[mm]
        if dd > ddUpperLimit or dd < 1 then
          dateProblem = "wrong date"
        end
      else
        dateProblem = "wrong format"
      end
    end
  end
  return dateProblem
end
--4 - VALIDATION
--print(checkDate("1988-02-30", tSpec["C.C"][10]))
tGenderOptions = {
  ["Laki-laki"] = true;
  --["Laki-Laki"] = true;
  ["Perempuan"] = true;
  --[""] = true;
}
tRiskIndicatorOptions = {
  ["Low"] = true;
  ["Medium"] = true;
  ["High"] = true;
  ["Unrated"] = true;
  --[""] = true;
}
function checkValidation(v, tSpecKeyI)
  local validationProblem = false
  local colName = tSpecKeyI.colName
  if string.upper(colName) == "GENDER_TP_CODE" then
    if not tGenderOptions[v] then
      validationProblem = "wrong value"
    end
  end
  if (string.upper(colName) == "EXT_IND_RISK_INDICATOR")
  or (string.upper(colName) == "EXT_ORG_RISK_INDICATOR") then
    if not tRiskIndicatorOptions[v] then
      validationProblem = "wrong value"
    end
  end
  return validationProblem
end
--5 - CODE TYPE
function checkCodeType(v, tSpecKeyI)
  local colName = tSpecKeyI.colName
  local isAvailable = false
  if (string.find(colName, "_TP_CD$") or string.find(colName, "_TP_CODE$")) and not string.find(colName, "^NULL_") then
    if (v) and (v ~= "") then
      local tRefCol = tRef[colName]
      for i, t in ipairs(tRefCol) do
        isAvailable = isAvailable or (t.typeCode == v)
        if isAvailable then break end
      end
    else
      isAvailable = "blank"
    end
  else
    isAvailable = "not type code"
  end
  --print(colName, v, isAvailable)
  return isAvailable, colName
end


--processing input sif files
for i, filename in ipairs(T_FILENAME) do
  print(string.format("[INFO] Processing file [%s].", filename))
  local isRejectFile = false
  if string.match(string.lower(filename), "reject") then isRejectFile = true end
  
  local fi = io.open(string.format("%s/%s", DATA_DIR, filename), "r")
  local lineCount = 1
  for line in fi:lines() do
  
  
    local tLine = split(line, "|")
    local key = string.format("%s.%s", tLine[1], tLine[2])
    local tSpecKey = tSpec[key]
    --print(line)
    --print(key, #tSpecKey, #t)
    local expectedSIFSize = isRejectFile and tSpecKey.origColCount or #tSpecKey
    local lineSize = #tLine
    if expectedSIFSize == lineSize then
      
      for i, v in ipairs(tLine) do
        local tSpecKeyI = tSpecKey[i]
        
        --CHECKING STARTS HERE
        local isExists = checkValueExists(v, tSpecKeyI, key)
        local dateProblem = checkDate(v, tSpecKeyI)
        local validationProblem = checkValidation(v, tSpecKeyI)
        local codeTypeProblem, colName = checkCodeType(v, tSpecKeyI)
        if (not isExists) --[[CHECKING EXCEPTION]]and (tSpecKeyI.colName ~= "CURR_CASH_VAL_AMT")--[[CHECKING EXCEPTION END]] then
          --print(string.format("[ERR_2_SPEC] File [%s] at line [%d] has no value for %s (col [%d]).", filename, lineCount, tSpecKeyI.colName, i))
        end
        if dateProblem --[=[CHECKING EXCEPTION]] and (not string.find(v, "^1260000000"))--[[CHECKING EXCEPTION END]]]=] then
          --print(string.format("[ERR_3_DATE] File [%s] at line [%d] has [%s] for %s (col [%d]) - %s.", filename, lineCount, dateProblem, tSpecKeyI.colName, i, v))
        end
        if validationProblem then
          --print(string.format("[ERR_4_VALI] File [%s] at line [%d] has [%s] for %s (col [%d]) - %s.", filename, lineCount, validationProblem, tSpecKeyI.colName, i, v))
        end
        if codeTypeProblem == false then
          print(string.format("%s (%s) not available in code table - %s at line [%d]", colName or "", v or "", filename, lineCount))
        end
      end
      
    else
      --[[CHECKING EXCEPTION]]if not(expectedSIFSize == 76 and lineSize == 77) then--[[CHECKING EXCEPTION END]]
      print(string.format("[ERR_1_SIZE] SIF size of [%s] at line [%d] is incorrect (%d instead of %d).", filename, lineCount, lineSize, expectedSIFSize))
      end
    end
    lineCount = lineCount + 1
    --if lineCount > 1 then break end--STOP
  
  
  end
  print()
  fi:close()
  --break--STOP
end
