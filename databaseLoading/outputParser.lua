fin = io.open("refTableInsert_R1V09.sql.out", "r")
fout = io.open("refTableInsert_R1V09.sql.out.analysis", "w")
c = 1
local sql
local t = {}
for line in fin:lines() do
  if string.find(line, "^INSERT") then
    sql = line
  elseif string.find(line, "^SQL Error:") then
    t[line] = t[line] or {}
    t[line][#t[line] + 1] = sql
  end
  
  c = c + 1
  --if c > 50 then break end
end

for cause, tSQL in pairs(t) do
  fout:write(cause .. "\t" .. #tSQL .. "\n")
  for i, sql in ipairs(tSQL) do
    fout:write("\t" .. sql .. "\n")
    --print("", sql)
  end--]]
end

