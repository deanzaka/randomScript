folder = "RefTableCSV_V0.21/"
fout = io.open(folder .. "refTableInsert_R1V21_cleanedup.sql", "w")
separator = "~"

tTableName = {
"CDADMINSYSTP.csv";
"CDIDTP.csv";
"CDINDUSTRYTP.csv";
"CDCONTMETHCAT.csv";
"CDCONTMETHTP.csv";
"CDCURRENCYTP.csv";
"CDORGTP.csv";
"CDADDRUSAGETP.csv";
"CDORGNAMETP.csv";
"CDMARITALSTTP.csv";
"CDCOUNTRYTP.csv";
"CDNAMEUSAGETP.csv";
"CDCLIENTIMPTP.csv";
"ERRREASON.csv";
"CDERRMESSAGETP.csv";
"CDGROUPINGTP.csv";
"CDHIERARCHYTP.csv";
"CDMISCVALUEATTRTP.csv";
"CDMISCVALUECAT.csv";
"CDMISCVALUETP.csv";
"CDPRODTP.csv";
"CDRELTP.csv";
"CDRELASSIGNTP.csv";
"CDPREFIXNAMETP.csv";
"CDCONTRACTSTTP.csv";
"CDEXTRELWITHBANKTP.csv";
"CDEXTOCCUPATIONTP.csv";
"CDHIERARCHYCATTP.csv";
"CDADMINFLDNMTP.csv";
"CDSOURCEIDENTTP.csv";
"CDPROVSTATETP.csv";
"CDCONTRACTROLETP.csv";
}

function cleanSpace(s)
  s = s or ""
  s = string.gsub(s, "'", "''")
  return string.gsub(string.gsub(s, "^%s*", "") or "", "%s*$", "")
end

function setn(t, n)
  return setmetatable(t, {__len = function() return n end})
end

c = 1
for _, tableName in ipairs(tTableName) do
  tableName = string.gsub(tableName, "%.csv$", "")
  local filename = folder .. tableName .. ".csv"
  tableName = "MDMADM." .. tableName
  fout:write("--" .. tableName .. "\n")
  fout:write("DELETE FROM " .. tableName .. ";\n")
  
  f = io.open(filename, "r")
  print()
  print(filename)
  local lineCount = 1
  local tHeader = {}
  local headerLen = nil
  local headerString = ""
  local tNullable = {}
  local tDataType = {}
  local tData = {}
  for line in f:lines() do
    local lineHasValue = string.find(line, "[^".. separator .. "]")
    local lineIsValid = true
    
    if lineHasValue then
      --print(">>>", line)
      local i = 1
      for v in string.gmatch(line .. separator, "([^" .. separator .. "]*)" .. separator) do
        
        v = cleanSpace(v)
        if lineCount == 1 then
          tHeader[i] = v
        else
          tData[i] = v
        end
        
        i = i + 1
      end
      
      if lineCount == 1 then
        for i = 2, #tHeader do
          local header = tHeader[i]
          
          if header == "" then
            --setn(tHeader, i-1)
            headerLen = i-1
            break
          end
          
          headerString = headerString .. string.format("%s", header)
          if i < #tHeader and tHeader[i+1] ~= "" then
            headerString = headerString .. ", "
          end
        end
        --print(">>>>>", headerString)
      end
      
      if string.lower(tData[1] or "") == "data type" then
        for i = 2, #tData do
          tDataType[i] = tData[i]
        end
      elseif string.lower(tData[1] or "") == "nullable" then
        for i = 2, #tData do
          tNullable[i] = string.upper(tData[i])
        end
      end
      
      if lineCount > 1 and tData[1] == "" then
        local sql = string.format("INSERT INTO %s (%s) VALUES (", tableName, headerString)
        for i = 2, (headerLen or #tHeader) do
          local data = tData[i]
          local quotedData
          if (data == "") then
            local header = tHeader[i]
            local dataType = tDataType[i]
            local isNullable = tNullable[i]
            if header == "LAST_UPDATE_DT" then
              quotedData = "CURRENT_TIMESTAMP"
            elseif isNullable == "Y" then
              quotedData = "NULL"
            elseif header == "VALUEATTR_TP_CD" then
              lineIsValid = nil
            else
              quotedData = "''"
              print(header, dataType, isNullable, tData[i])
            end
          elseif (string.upper(data) == "NULL") then
            quotedData = "NULL"
          elseif (string.upper(data) == "CURRENT_TIMESTAMP") then
            quotedData = "CURRENT_TIMESTAMP"
          elseif (string.find(data, "&")) then
            quotedData = "'"
            for s in string.gmatch(data .. "&", "([^&]+)&") do
              quotedData = quotedData .. s .. "' || chr(38) || '"
            end
            quotedData = string.gsub(quotedData, " || chr%(38%) || '$", "")
            --print(quotedData)
          else
            quotedData = string.format("'%s'", data)
          end
          --print(tHeader[i], data)
          sql = sql .. quotedData
          if i < (headerLen or #tHeader) then
            sql = sql .. ", "
          end
        end
        sql = sql .. ");"
        if lineIsValid then
          fout:write(sql .. "\n")
        end
      end
      
    end
    
    lineCount = lineCount + 1
    --[[if lineCount > 10 then
      break
    end--]]
    
  end
  
  f:close()
  
  fout:write("COMMIT;\n")
  fout:write("\n\n")
  
  c = c + 1
  --[[if c > 5 then
    break
  end--]]
end

fout:close()