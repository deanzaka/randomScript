folder = "RefTableCSV_0.19/"
fout = io.open(folder .. "refTable_R1V19_cleanedup.lua", "w")
separator = "~"

tTableName = {
"CDADDRUSAGETP";
"CDADMINFLDNMTP";
"CDADMINSYSTP";
"CDCLIENTIMPTP";
"CDCONTMETHCAT";
"CDCONTMETHTP";
"CDCONTRROLETP";
"CDCONTRACTSTTP";
"CDCOUNTRYTP";
"CDCURRENCYTP";
--"CDERRMESSAGETP";
"CDOCCUPATIONTP";
"CDRELWITHBANKTP";
"CDGROUPINGTP";
--"CDHIERARCHYCATTP";
--"CDHIERARCHYTP";
"CDIDTP";
"CDINDUSTRYTP";
"CDMARITALSTTP";
--"CDMISCVALUEATTRTP";
--"CDMISCVALUECAT";
--"CDMISCVALUETP";
"CDNAMEUSAGETP";
"CDORGNAMETP";
"CDORGTP";
--"CDPREFIXNAMETP";
"CDPRODTP";
"CDPROVSTATETP";
"CDRELASSIGNTP";
"CDRELTP";
"CDSOURCEIDENTTP";
--"ERRREASON";
--]]
}

function cleanSpace(s)
  s = s or ""
  s = string.gsub(s, "'", "''")
  return string.gsub(string.gsub(s, "^%s*", "") or "", "%s*$", "")
end
function clearTrail(s)
  --replace variation of spaces into regular space
  s = string.gsub(s, "�", " ")--nbsp to regular space
  --remove spaces at the beginning and the end of the phrase
  s = string.gsub(s, "^[%s]+", "")
  s = string.gsub(s, "[%s]+$", "")
  --remove multiple spaces
  s = string.gsub(s, "([%s])[%s]+", "%1")
  --remove spaces after and before parentheses
  s = string.gsub(s, "%(%s+", "(")
  s = string.gsub(s, "%s+%)", ")")
  --replace variation of dashes to regular dashes
  s = string.gsub(s, "[%�%�%�]", "-")
  --remove spaces after and before dash
  s = string.gsub(s, "%s*%-%s*", "-")
  return s
end

fout:write("tRef = {}\n")

c = 1
for _, tableName in ipairs(tTableName) do
  local filename = folder .. tableName .. ".csv"
  fout:write("--" .. tableName .. "\n")
  
  f = io.open(filename, "r")
  print()
  print(filename)
  local lineCount = 1
  local dataCount = 1
  local tHeader = {}
  local headerLen = nil
  local tData = {}
  local keyIndex
  for line in f:lines() do
    local lineHasValue = string.find(line, "[^".. separator .. "]")
    local lineIsValid = true
    
    if lineHasValue then

      local i = 1
      for v in string.gmatch(line .. separator, "([^" .. separator .. "]*)" .. separator) do
        
        v = clearTrail(v)
        if lineCount == 1 then
          tHeader[i] = v
        else
          tData[i] = v
        end
        
        i = i + 1
      end
      
      if lineCount == 1 then
        for i = 2, #tHeader do
        
          local header = tHeader[i]
          if header == "" then
            headerLen = i-1
            break
          end
          
          local headerContent = string.gsub(string.match(header, "(.+)_CD$") or "", "_", "")
          if string.find(header, "_CD$") and header ~= "LANG_TP_CD" then
            if ("CD" .. headerContent) == tableName then
              fout:write(string.format("tRef[\"%s\"] = {\n", header))
              keyIndex = i
            end
          end
          
        end
      end
      
      if lineCount > 1 and tData[1] == "" then
        if data ~= "" then
        
          local infoIndex
          for i = 2, (headerLen or #tHeader) do
            local header = tHeader[i]
            if string.find(header, "^NAME$") or (header == "TO_FROM_NAME") then
              local typeCode = tData[keyIndex]
              local info = tData[i]
              fout:write(string.format("{typeCode = \"%s\"; name = \"%s\"};\n", typeCode, info))
            end
          end
          
          dataCount = dataCount + 1
          
        end
      end
    end
    
    lineCount = lineCount + 1
    --[[if dataCount > 3 then
      break
    end--]]
    
  end
  
  f:close()
  
  print()
  fout:write("}\n\n")
  
  c = c + 1
  --[[if c > 5 then
    break
  end--]]
end

fout:close()