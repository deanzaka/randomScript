function clearTrail(s)
  --replace variation of spaces into regular space
  s = string.gsub(s, "�", " ")--nbsp to regular space
  --remove spaces at the beginning and the end of the phrase
  s = string.gsub(s, "^[%s]+", "")
  s = string.gsub(s, "[%s]+$", "")
  --remove multiple spaces
  s = string.gsub(s, "([%s])[%s]+", "%1")
  --remove spaces after and before parentheses
  s = string.gsub(s, "%(%s+", "(")
  s = string.gsub(s, "%s+%)", ")")
  --replace variation of dashes to regular dashes
  s = string.gsub(s, "[%�%�%�]", "-")
  --remove spaces after and before dash
  s = string.gsub(s, "%s*%-%s*", "-")
  return s
end

separator = "\t"

fout = io.open("out.txt", "w")
f = io.open("in.txt", "r")
lineCount = 1
itemCount = 1
tHeader = {}
for line in f:lines() do
  
  --[[print(line)
  print(clearTrail(line))
  print()--]]
  
  for v in string.gmatch(line .. "\t", "([^\t]+)\t") do
    fout:write(clearTrail(v) .. "\t")
  end
  fout:write("\n")
  
  
  lineCount = lineCount + 1
  --if lineCount > 10 then break end
end
f:close()
fout:close()
