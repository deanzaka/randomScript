#!/bin/bash
#----------
# checkUnprocessedFile receives a list of input files. It will check the log directory
# whether the log file for that input SIF file has been generated. The unprocessed files would be printed out.
#----------

#MACRO
SCRIPT_ROOT_DIR='/dstage/verification';

#START
for loc in `echo $1`; do
  filename=`echo $loc | awk -F "/" '{print $NF}' | sed "s/\.sif.*$//"`;
  dir=`echo $loc | sed "s/\/"$filename"\.sif.*//"`;
  subdir=`echo $dir | sed "s/\/$//" | awk -F "/" '{print $NF}'`;
  
  logFileLocDL=$SCRIPT_ROOT_DIR/logDL/$subdir/$filename.log;
  logGzFileLocDL=$logFileLocDL.gz;
  logFileLoc2=$SCRIPT_ROOT_DIR/log/$subdir/$filename.log;
  logFileLoc2=$logFileLoc2.gz;
  sifFileLoc=$dir/$filename.sif;
  sifGzFileLoc=$sifFileLoc.gz;
  
  if [ -e $logFileLocDL ]; then
    gzip -9 $logFileLocDL
  elif [ ! -e $logGzFileLocDL ] && [ -e $logGzFileLoc2 ]; then
    if [ -e $sifFileLoc ]; then
      ls -lart $sifFileLoc;
    else
      ls -lart $sifGzFileLoc;
    fi;
  fi;
done;