local filename = arg[1] or ""
local filePath = string.gsub(filename, "%.sif.*$", ".log")

isExist = os.execute(string.format("if [ -e %s ]; then return 0; else return -1; fi;", filePath))

local tSummary = {
  ["01_fieldCount"] = {};
  ["02_nullable"] = {};
  ["03_dateFormat"] = {};
  ["04_validation"] = {};
  ["05_codeType"] = {};
  ["06_controlChar"] = {};
}

if isExist == 0 then
  local fi = io.open(filePath)
  local category
  for line in fi:lines() do
    category = string.match(line, "^===([^=]+)===") or category
    local log = string.match(line, "^%d+%s*(.+)")
    if category and log then
      tSummary[category][log] = true
    end
  end
  fi:close()
  
  for category, tLog in pairs(tSummary) do
    local isFirst = true
    for log in pairs(tLog) do
      if isFirst then
        print(string.format("===%s===", category))
        isFirst = false
      end
      print(log)
    end
  end
else
  print(string.format("File %s doesn't exist.", filePath))
end
