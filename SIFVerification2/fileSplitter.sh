#!/bin/bash
#----------
# fileSplitter receives a filename as the parameter or a list of filenames to be processed; e.g.:
#   ./fileSplitter /dstage/STANDARDIZATION/SIF/TARGET/PARTY/EMAS_PartyContact_03082015.sif.gz
#   ./fileSplitter "/dstage/STANDARDIZATION/SIF/TARGET/PARTY/EMAS_PartyContact_03082015.sif.gz
#       /dstage/STANDARDIZATION/SIF/TARGET/PARTY/EMAS_PartyMiscValue_03082015.sif.gz
#       /dstage/STANDARDIZATION/SIF/TARGET/PARTY/AXA_RLS_PartyContact_03082015.sif.gz"
# Choose between split based on the filesize (MAX_FILESIZE)
# or by the number of fileparts to be generated (DEFAULT_SPLIT_COUNT)
#----------
#MACRO
SCRIPT_ROOT_DIR='/dstage/verification';
#MAX_FILESIZE=2000000;#2GB file limit size
DEFAULT_SPLIT_COUNT=3;

#START
for loc in `echo $1`; do
  filename=`echo $loc | awk -F "/" '{print $NF}' | sed "s/\.sif.*$//"`;
  dir=`echo $loc | sed "s/\/"$filename"\.sif.*//"`;
  #subdir=`echo $dir | sed "s/\/$//" | awk -F "/" '{print $NF}'`;
  ext=`echo $loc | awk -F "/" '{print $NF}' | sed "s/"$filename"\.//"`;
  
  sifFileLoc=$dir/$filename.sif;
  sifGzFileLoc=$sifFileLoc.gz;
  
  c=0;
  fileSize=`ls -lart $loc | awk '{print $5}'`
  if [[ $MAX_FILESIZE ]]; then
    splitCount=$(($fileSize/$MAX_FILESIZE));
    echo "split by fileSize="$splitCount;
  else
    splitCount=$DEFAULT_SPLIT_COUNT;
    echo "split by fileCount="$splitCount;
  fi;
  lineCount=`wc -l $loc | awk '{print $1}'`;
  splitSize=$(($lineCount/$splitCount));
  while [[ $c -lt $splitCount ]]; do
    partIndex=$(($splitCount - $c));
    partFilename=$filename.$ext".part"$partIndex;
    c=$(($c + 1));
    if [ ! $c -eq $splitCount ]; then
      tail -$(($c*$splitSize)) $loc | head -$splitSize > $partFilename;
    else
      head -$(($lineCount - ($c - 1)*$splitSize)) $loc > $partFilename;
    fi;
  done;
done;