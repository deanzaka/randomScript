#!/bin/bash
#----------
# 
#----------

#MACRO
SCRIPT_ROOT_DIR='/dstage/verification';

#START
for loc in `find /dstage/STANDARDIZATION/SIF/TARGET -type f -name *.sif | egrep "Delta|Combined|Reject" | egrep -v "_PartyContact_|_PersonName_|_OrganizationName_"`; do
  filename=`echo $loc | awk -F "/" '{print $NF}' | sed "s/\.sif.*$//"`;
  dir=`echo $loc | sed "s/\/"$filename"\.sif.*//"`;
  subdir=`echo $dir | sed "s/\/$//" | awk -F "/" '{print $NF}'`;
  
  luaVer8Loc=$SCRIPT_ROOT_DIR/sifVerification8.lua;
  sifFileLoc=$dir/$filename.sif;
  #sifGzFileLoc=$sifFileLoc.gz;
  logFileDir=$SCRIPT_ROOT_DIR/logDL/$subdir;
  logFileLoc=$logFileDir/$filename.log;
  logGzFileLoc=$logFileLoc.gz;
  
  echo $logFileDir;
  if [ ! -d $logFileDir ]; then
    mkdir -p $logFileDir;
  fi;
  
  if [ -e $logGzFileLoc ]; then
    rm $logGzFileLoc;
  fi;
  
  if [ -e $sifGzFileLoc]; then
    #gunzip $sifGzFileLoc;
    lua $luaVer8Loc $sifFileLoc;
    #gzip -9 $sifFileLoc;
    gzip -9 $logFileLoc;
  else
    echo $sifGzFileLoc;
  fi;
done;
--process DL log file
for loc in `find /dstage/STANDARDIZATION/SIF/TARGET -type f -name *.sif | egrep "Delta|Combined|Reject" | egrep -v "_PartyContact_|_PersonName_|_OrganizationName_"`; do
  filename=`echo $loc | awk -F "/" '{print $NF}' | sed "s/\.sif$//"`;
  dir=`echo $loc | sed "s/\/"$filename".sif//"`;
  subdir=`echo $dir | sed "s/\/$//" | awk -F "/" '{print $NF}'`;
  
  rootDir='/dstage/verification';
  luaSummariserLoc=$rootDir/logSummariser.lua;
  logFileLoc=$rootDir/logDL/$subdir/$filename.log;
  logGzFileLoc=$logFileLoc.gz;
  sifFileLoc=$dir/$filename.sif;
  summaryFileLoc=$rootDir/summary2DL.log;
  
  if [ -e $logGzFileLoc ]; then
    gunzip $logGzFileLoc;
    echo $sifFileLoc >> $summaryFileLoc;
    lua $luaSummariserLoc $logFileLoc >> $summaryFileLoc;
    if [ -e $logFileLoc ]; then gzip -9 $logFileLoc; fi;
    #echo "a" > .tmp; rm .tmp;
  else
    echo "$logGzFileLoc";
  fi;
done;