filename=$1;
lineCount=$2

if [ -e $filename ]; then
  cat /dev/null > $filename;
fi;

c=0;
while [[ $c -lt $lineCount ]]; do
  c=$(($c + 1));
  echo $c >> $filename;
done;