tRef["GENDER_TP_CODE"] = {
{typeCode = "Laki-laki"; name = "Laki-laki"};
{typeCode = "Perempuan"; name = "Perempuan"};
}

tRef["ACCE_COMP_TP_CD"] = {
{typeCode = "1"; name = "14.4K Baud"};
{typeCode = "2"; name = "28.8K Baud"};
{typeCode = "3"; name = "56K Baud"};
{typeCode = "4"; name = "T-1 Carrier Line"};
{typeCode = "5"; name = "T-3 Carrier Line"};
{typeCode = "6"; name = "Cable"};
{typeCode = "7"; name = "High Speed Modem"};
}

tRef["AGE_VER_DOC_TP_CD"] = {
{typeCode = "1"; name = "Birth Certificate"};
{typeCode = "2"; name = "Passport"};
{typeCode = "3"; name = "Driver's Licence"};
{typeCode = "4"; name = "Adoption Records"};
{typeCode = "5"; name = "Court Order"};
{typeCode = "6"; name = "Citizenship"};
{typeCode = "7"; name = "Military Service Record"};
{typeCode = "8"; name = "Statutory Declaration"};
}

tRef["AGREEMENT_ST_TP_CD"] = {
{typeCode = "1"; name = "Draft"};
{typeCode = "2"; name = "Normal"};
{typeCode = "3"; name = "Suspended"};
{typeCode = "4"; name = "Terminated"};
}

tRef["AGREEMENT_TP_CD"] = {
{typeCode = "1"; name = "Value Package"};
{typeCode = "2"; name = "Employment Agreement"};
{typeCode = "3"; name = "Vendor Agreement"};
}

tRef["ALERT_SEV_TP_CD"] = {
{typeCode = "1"; name = "High"};
{typeCode = "2"; name = "Medium"};
{typeCode = "3"; name = "Low"};
}

tRef["ARRANGEMENT_TP_CD"] = {
{typeCode = "1"; name = "Common Disaster Arrangement"};
{typeCode = "10"; name = "No Service Arrangement"};
{typeCode = "11"; name = "Claim Service Only Arrangement"};
{typeCode = "12"; name = "Admin Service Only Arrangement"};
}

tRef["BILL_TP_CD"] = {
{typeCode = "1"; name = "No Billing"};
{typeCode = "2"; name = "Regular Billing"};
{typeCode = "3"; name = "Paid in Advance"};
{typeCode = "4"; name = "List Billing"};
{typeCode = "5"; name = "Payroll Deduction"};
{typeCode = "6"; name = "Electronic Funds Transfer"};
{typeCode = "7"; name = "Government Allotment"};
{typeCode = "8"; name = "Credit Card Billing"};
{typeCode = "9"; name = "Irregular Billing"};
{typeCode = "10"; name = "Equal Billing"};
}

tRef["BUY_SELL_AGR_TP_CD"] = {
{typeCode = "1"; name = "Entity Purchase Agreement"};
{typeCode = "2"; name = "Cross Purchase Agreement"};
{typeCode = "3"; name = "Hybrid Agreement"};
}

tRef["CLIENT_POTEN_TP_CD"] = {
{typeCode = "1"; name = "High"};
{typeCode = "2"; name = "Medium"};
{typeCode = "3"; name = "Low"};
{typeCode = "4"; name = "Growth"};
}

tRef["CLIENT_ST_TP_CD"] = {
{typeCode = "1"; name = "Customer"};
{typeCode = "2"; name = "Prospect"};
{typeCode = "3"; name = "Deceased"};
{typeCode = "4"; name = "Inactive"};
{typeCode = "5"; name = "Disabled"};
}

tRef["CONTR_COMP_TP_CD"] = {
{typeCode = "1"; name = "Base Account"};
{typeCode = "2"; name = "Service Agreement"};
{typeCode = "3"; name = "Fee"};
{typeCode = "4"; name = "Rebate"};
}

tRef["LANG_TP_CD"] = {
{typeCode = "100"; name = "English"};
{typeCode = "200"; name = "Fran�ais"};
{typeCode = "300"; name = "Espa�ol"};
{typeCode = "400"; name = "??(??)"};
{typeCode = "500"; name = "????"};
{typeCode = "600"; name = "Deutsch"};
{typeCode = "700"; name = "???"};
{typeCode = "800"; name = "???"};
{typeCode = "900"; name = "Italiano"};
{typeCode = "1000"; name = "Portugu�s (Brasil)"};
{typeCode = "1100"; name = "Arabic"};
{typeCode = "1200"; name = "Czech"};
{typeCode = "1300"; name = "Danish"};
{typeCode = "1400"; name = "Dutch"};
{typeCode = "1500"; name = "Finnish"};
{typeCode = "1600"; name = "????????"};
{typeCode = "1700"; name = "Hebrew"};
{typeCode = "1800"; name = "Hungarian"};
{typeCode = "1900"; name = "Norwegian (Bokmal)"};
{typeCode = "2000"; name = "Polski"};
{typeCode = "2100"; name = "Portuguese (Portugal)"};
{typeCode = "2200"; name = "???????"};
{typeCode = "2300"; name = "Swedish"};
{typeCode = "2400"; name = "T�rk�e"};
{typeCode = "2500"; name = "Hindi"};
{typeCode = "2600"; name = "Thai"};
{typeCode = "2700"; name = "Albanian"};
{typeCode = "2800"; name = "Bulgarian"};
{typeCode = "2900"; name = "Belarusian"};
{typeCode = "3000"; name = "Catalan"};
{typeCode = "3100"; name = "Croatian"};
{typeCode = "3200"; name = "Estonian"};
{typeCode = "3300"; name = "Icelandic"};
{typeCode = "3400"; name = "Latvian"};
{typeCode = "3500"; name = "Lithuanian"};
{typeCode = "3600"; name = "Mecedonian"};
{typeCode = "3700"; name = "Romanian"};
{typeCode = "3800"; name = "Serbian (Cyrillic)"};
{typeCode = "3900"; name = "Serbian (Latin)"};
{typeCode = "4000"; name = "Slovak"};
{typeCode = "4100"; name = "Slovenian"};
{typeCode = "4200"; name = "Ukrainian"};
}

tRef["END_REASON_TP_CD"] = {
{typeCode = "1"; name = "Retired"};
{typeCode = "2"; name = "Terminated"};
{typeCode = "3"; name = "Person Deceased"};
{typeCode = "4"; name = "Organization Dissolved"};
{typeCode = "5"; name = "Expired"};
{typeCode = "6"; name = "Related entity deactivated"};
{typeCode = "7"; name = "Inactivated due to party collapse"};
{typeCode = "8"; name = "Inactivated due to party split"};
{typeCode = "9"; name = "Inactivated due to product collapse"};
{typeCode = "10"; name = "Inactivated due to product split"};
{typeCode = "11"; name = "Inactivated due to party undo collapse"};
{typeCode = "12"; name = "Inactivated due to product undo collapse"};
}

tRef["FREQ_MODE_TP_CD"] = {
{typeCode = "1"; name = "Annual"};
{typeCode = "2"; name = "Semi-Annual"};
{typeCode = "3"; name = "Quarterly"};
{typeCode = "4"; name = "Semi-Monthly"};
{typeCode = "5"; name = "Monthly"};
{typeCode = "6"; name = "Weekly"};
{typeCode = "7"; name = "Bi-Weekly"};
{typeCode = "8"; name = "Daily"};
}

tRef["GENERATION_TP_CD"] = {
{typeCode = "1"; name = "The First"};
{typeCode = "2"; name = "The Second"};
{typeCode = "3"; name = "The Third"};
{typeCode = "4"; name = "The Fourth"};
{typeCode = "5"; name = "Junior"};
{typeCode = "6"; name = "Senior"};
}

tRef["HIGHEST_EDU_TP_CD"] = {
{typeCode = "1"; name = "Secondary School Certificate"};
{typeCode = "3"; name = "College Diploma"};
{typeCode = "4"; name = "University Degree"};
{typeCode = "5"; name = "Master Degree"};
{typeCode = "6"; name = "Doctorate Degree"};
{typeCode = "7"; name = "Bachelor Degree"};
}

tRef["ID_STATUS_TP_CD"] = {
{typeCode = "1"; name = "Applied for identification"};
{typeCode = "2"; name = "Active"};
{typeCode = "3"; name = "Inactive"};
{typeCode = "4"; name = "Expired"};
{typeCode = "5"; name = "Certified"};
{typeCode = "6"; name = "Not Certified"};
}

tRef["LOB_REL_TP_CD"] = {
{typeCode = "1"; name = "Owner"};
}

tRef["LOB_TP_CD"] = {
{typeCode = "1"; name = "Life Insurance"};
{typeCode = "2"; name = "P and C - Personal Lines"};
{typeCode = "3"; name = "P and C - Commercial Lines"};
{typeCode = "5"; name = "Group Insurance"};
{typeCode = "6"; name = "Retail Banking"};
{typeCode = "7"; name = "Commercial Banking"};
{typeCode = "8"; name = "Credit Card Portfolio"};
{typeCode = "9"; name = "Insurance Brokerage"};
}

tRef["METHOD_ST_TP_CD"] = {
{typeCode = "1"; name = "Listed"};
{typeCode = "2"; name = "Unlisted"};
{typeCode = "3"; name = "Not Published"};
}

tRef["PPREF_REASON_TP_CD"] = {
{typeCode = "1"; name = "Not Interested"};
{typeCode = "2"; name = "Interested"};
{typeCode = "3"; name = "Do Not Send to Priv. Residence"};
{typeCode = "4"; name = "Office e-mail cannot be used for soliciting"};
}

tRef["PREFIX_NAME_TP_CD"] = {
{typeCode = "1"; name = "Colonel"};
{typeCode = "2"; name = "Doctor"};
{typeCode = "3"; name = "Father"};
{typeCode = "4"; name = "General"};
{typeCode = "5"; name = "Honorable"};
{typeCode = "6"; name = "Lieutenant"};
{typeCode = "7"; name = "Lieutenant Colonel"};
{typeCode = "8"; name = "Lieutenant General"};
{typeCode = "9"; name = "Major General"};
{typeCode = "10"; name = "Major"};
{typeCode = "11"; name = "Master Sergeant"};
{typeCode = "12"; name = "Miss"};
{typeCode = "13"; name = "Mother"};
{typeCode = "14"; name = "Mr."};
{typeCode = "15"; name = "Mrs."};
{typeCode = "16"; name = "Ms."};
{typeCode = "17"; name = "Pastor"};
{typeCode = "18"; name = "Prince"};
{typeCode = "19"; name = "Princess"};
{typeCode = "20"; name = "Professor"};
{typeCode = "21"; name = "Rabbi"};
{typeCode = "22"; name = "Reverend Father"};
{typeCode = "23"; name = "Reverend"};
{typeCode = "24"; name = "Sergeant"};
{typeCode = "25"; name = "Sergeant Major"};
{typeCode = "26"; name = "Sister"};
{typeCode = "27"; name = "Master"};
}

tRef["PRIORITY_TP_CD"] = {
{typeCode = "1"; name = "Medium"};
{typeCode = "2"; name = "High"};
{typeCode = "3"; name = "Low"};
{typeCode = "100"; name = "One"};
{typeCode = "200"; name = "Two"};
{typeCode = "300"; name = "Three"};
{typeCode = "10001"; name = "Batch priority 1"};
{typeCode = "10002"; name = "Batch priority 2"};
{typeCode = "10003"; name = "Batch priority 3"};
{typeCode = "10004"; name = "Batch priority 4"};
{typeCode = "10005"; name = "Batch priority 5"};
{typeCode = "10006"; name = "Batch priority 6"};
{typeCode = "10007"; name = "Batch priority 7"};
{typeCode = "10008"; name = "Batch priority 8"};
{typeCode = "10009"; name = "Batch priority 9"};
{typeCode = "10010"; name = "Batch priority 10"};
{typeCode = "10011"; name = "Batch priority 11"};
{typeCode = "10012"; name = "Batch priority 12"};
{typeCode = "10013"; name = "Batch priority 13"};
{typeCode = "10014"; name = "Batch priority 14"};
{typeCode = "10015"; name = "Batch priority 15"};
{typeCode = "10016"; name = "Batch priority 16"};
{typeCode = "10017"; name = "Batch priority 17"};
{typeCode = "10018"; name = "Batch priority 18"};
{typeCode = "10019"; name = "Batch priority 19"};
{typeCode = "10020"; name = "Batch priority 20"};
}

tRef["RESIDENCE_TP_CD"] = {
{typeCode = "1"; name = "Apartment"};
{typeCode = "2"; name = "Detached House"};
{typeCode = "3"; name = "Low Rise Condominium"};
{typeCode = "4"; name = "High Rise Condominium"};
{typeCode = "5"; name = "Semi Detached House"};
{typeCode = "6"; name = "Townhouse"};
{typeCode = "7"; name = "Duplex"};
{typeCode = "8"; name = "Converted Loft"};
{typeCode = "9"; name = "Suite"};
{typeCode = "10"; name = "Unit"};
{typeCode = "11"; name = "Office"};
{typeCode = "12"; name = "Post Office Box"};
}

tRef["RPTING_FREQ_TP_CD"] = {
{typeCode = "1"; name = "Annually"};
{typeCode = "2"; name = "Monthly"};
{typeCode = "3"; name = "Semi-annually"};
{typeCode = "4"; name = "Quarterly"};
}

tRef["SERVICE_LEVEL_TP_CD"] = {
{typeCode = "1"; name = "Service during business hours only"};
{typeCode = "2"; name = "Service 24/7 through Call Centre"};
}

tRef["SHARE_DIST_TP_CD"] = {
{typeCode = "1"; name = "Equal Shares"};
{typeCode = "2"; name = "10% Share"};
{typeCode = "3"; name = "15% Share"};
{typeCode = "4"; name = "20% Share"};
{typeCode = "5"; name = "25% Share"};
{typeCode = "6"; name = "30% Share"};
{typeCode = "7"; name = "35% Share"};
{typeCode = "8"; name = "40% Share"};
{typeCode = "9"; name = "45% Share"};
{typeCode = "10"; name = "50% Share"};
{typeCode = "11"; name = "55% Share"};
{typeCode = "12"; name = "60% Share"};
{typeCode = "13"; name = "65% Share"};
{typeCode = "14"; name = "70% Share"};
{typeCode = "15"; name = "75% Share"};
{typeCode = "16"; name = "80% Share"};
{typeCode = "17"; name = "85% Share"};
{typeCode = "18"; name = "90% Share"};
{typeCode = "19"; name = "95% Share"};
{typeCode = "20"; name = "100% Share"};
}

tRef["TERMINATION_REASON_TP_CD"] = {
{typeCode = "1"; name = "Agreement Terms & Conditions violation"};
}

tRef["UNDEL_REASON_TP_CD"] = {
{typeCode = "1"; name = "Moved"};
{typeCode = "2"; name = "Postal Code is incorrect"};
{typeCode = "3"; name = "Apartment Number is incorrect"};
{typeCode = "4"; name = "Street Address is incorrect"};
}


--===typeCode with typeCategory===
tRef["ALERT_TP_CD"] = {
{typeCode = "1"; typeCat = "1"; name = "Domestic Violence"};
{typeCode = "2"; typeCat = "1"; name = "Class Action"};
{typeCode = "3"; typeCat = "1"; name = "Non-Disclosure"};
{typeCode = "4"; typeCat = "1"; name = "Privacy Notice"};
{typeCode = "5"; typeCat = "1"; name = "Type Limit"};
{typeCode = "6"; typeCat = "2"; name = "Category Limit"};
{typeCode = "7"; typeCat = "3"; name = "Services"};
{typeCode = "8"; typeCat = "3"; name = "General"};
{typeCode = "9"; typeCat = "4"; name = "Task Comment"};
}

tRef["DOMAIN_VALUE_TP_CD"] = {
{typeCode = "1"; typeCat = "1"; name = "Domain Value"};
{typeCode = "2"; typeCat = "1"; name = "Integer Value"};
{typeCode = "3"; typeCat = "1"; name = "Decimal Value"};
{typeCode = "4"; typeCat = "1"; name = "Date Value"};
{typeCode = "5"; typeCat = "1"; name = "Time Value"};
{typeCode = "6"; typeCat = "1"; name = "Timestamp Value"};
}

tRef["MISCVALUE_TP_CD"] = {
{typeCode = "12800000001"; typeCat = "1"; name = "Customer Additional Information"};
{typeCode = "12800000002"; typeCat = "2"; name = "Channel Status Facility"};
{typeCode = "12800000003"; typeCat = "3"; name = "Contact Address and Phone"};
{typeCode = "12800000004"; typeCat = "4"; name = "Contact Name, Branch, Job Position, Prefix and Sufix"};
}

tRef["PPREF_TP_CD"] = {
{typeCode = "1"; typeCat = "1"; name = "Privacy"};
{typeCode = "2"; typeCat = "3"; name = "Insurance"};
{typeCode = "3"; typeCat = "4"; name = "Retail Banking"};
{typeCode = "4"; typeCat = "5"; name = "Channel Preference"};
}
