filename = arg[1]
print(filename)
file = io.open(filename, "r")
filesize, err = file:seek("end")
if not filesize then
  print(err)
else
  print(filesize)
end
file:close()
