#!/bin/bash
inputdir='/home/deanzaka/Downloads/TARGET/CONTACT';
outputdir='/home/deanzaka/Downloads/OUTPUT/CONTACT';
runbatchpath='.'

cond=(Combined_Contact Delta_PersonName\|Delta_OrganizationName Delta_PartyAddress Delta_PartyContactMethod Delta_PartyIdentification)

echo '#!/bin/bash' > $runbatchpath/running_batch.sh; echo;
for i in "${cond[@]}"; do
	echo "echo 'running $i'" >> $runbatchpath/running_batch.sh;
	for loc in `find $inputdir -type f -name *.sif | egrep "$i"`; do
	  filename=`echo $loc | awk -F "/" '{print $NF}' | sed "s/\.sif$//"`;
	  dir=`echo $loc | sed "s/"$filename".sif//"`;
	  subdir=`echo $dir | sed "s/\/$//" | awk -F "/" '{print $NF}'`;
	  
	  echo ./runbatch.sh $inputdir/$filename.sif $outputdir/$filename;
	  echo "./runbatch.sh $inputdir/$filename.sif $outputdir/$filename;" >> $runbatchpath/running_batch.sh;
	done;
done
chmod 744 running_batch.sh;
